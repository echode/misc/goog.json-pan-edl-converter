# goog.json PAN EDL Converter

Converts the goog.json to PaloAlto EDL format

## Running in Docker

This is the easiest way to get things up and running

### Build

Make sure you have docker and buildx installed, then run ``docker build . --tag googedl:latest``.

_If you need custom CA Certificates, you can add them in the trusted-certs.pem file before you build the container._

### Run

For testing purposes, run ``docker run --name googedl-temp --publish 8000:8000 --rm googedl:latest``.

In production you should probably configure the amount of workers and put the endpoint behind a reverse proxy.

To run the container in the background, run

```
docker network create googedl
docker run --name googedl --network googedl --publish 127.0.0.1:8000:8000 --restart unless-stopped --detach googedl:latest 16
```

The number at the end is the amount of workers. This should probably be between 2-4 times the number of CPU cores on the server. (default is 4)

## Running standalone

You can also run the server directly on most linux distros.

### Installing dependencies

Make sure you have python3.4 or later and pip installed on your system, then run

```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### Run

For testing, you can run ``flask --app googedl run --debug`` and the server will listen localhost port 5000 with debug mode enabled.

In production we need to run the app through a WSGI server.

There are many WSGI servers to choose from, but for simplicity this project includes [Gunicorn](https://gunicorn.org) in its dependencies.

To start the server using Gunicorn, you can run

```
source .venv/bin/activate
gunicorn googedl:server --bind=0.0.0.0:8000 --workers=16
```

The number of workers should probably be between 2-4 times the number of CPU cores on the server.

## Usage

Just add the endpoint as source for a IP List EDL in your firewall.
You can also append /ipv4 or /ipv6 to limit the output.