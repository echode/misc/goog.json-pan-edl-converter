import requests
from flask import Flask, abort
from werkzeug.exceptions import HTTPException, BadRequest

s = '\r\n'

### Functions ###

def fetch(filter: str = 'all') -> list:
	ipv4 = []
	ipv6 = []
	edl = []

	try:
		request = requests.get('https://www.gstatic.com/ipranges/goog.json')
		response = request.json()
		for prefix in response['prefixes']:
			if 'ipv4Prefix' in prefix:
				ipv4.append(prefix['ipv4Prefix'])
			if 'ipv6Prefix' in prefix:
				ipv6.append(prefix['ipv6Prefix'])
	except:
		print(f'Something broke while fetching goog.json')
		abort(500)

	if filter == 'ipv4' or filter == 'all':
		edl = edl + ipv4
	if filter == 'ipv6' or filter == 'all':
		edl = edl + ipv6

	return edl

### Application ###

server = Flask(__name__)

@server.errorhandler(BadRequest)
def err_bad_request(error):
	return error, 400

@server.route('/', methods=['GET'])
def listall():
	edl = fetch()
	return s.join(edl)

@server.route('/ipv4', methods=['GET'])
def listipv4():
	edl = fetch('ipv4')
	return s.join(edl)

@server.route('/ipv6', methods=['GET'])
def listipv6():
	edl = fetch('ipv6')
	return s.join(edl)