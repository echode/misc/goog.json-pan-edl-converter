FROM alpine:3.19
ADD src/* /app/
ADD trusted-certs.pem /etc/ssl/certs/
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN <<EOR
  adduser -D www
  cat /etc/ssl/certs/trusted-certs.pem | tee -a /etc/ssl/certs/ca-certificates.crt
  apk add --no-cache py3-pip
  python3 -m venv $VIRTUAL_ENV
  cat /etc/ssl/certs/trusted-certs.pem | tee -a ${VIRTUAL_ENV}/lib/python3.11/site-packages/pip/_vendor/certifi/cacert.pem
  pip install -r /app/requirements.txt
  cat /etc/ssl/certs/trusted-certs.pem | tee -a ${VIRTUAL_ENV}/lib/python3.11/site-packages/certifi/cacert.pem
EOR
USER www
WORKDIR /app
ENTRYPOINT ["gunicorn", "googedl:server", "-b", "0.0.0.0:8000", "-w"]
CMD [ "4" ]
EXPOSE 8000
